//#include <linux/pwm.h>
//static struct pwm_device test;

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/gpio/consumer.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/pwm.h>
#include <linux/compat.h>

#define MISC_NAME "MagIA-servoctl"
#define MAGIA_MAGIC 0xDE

#define SETTINGS_IOCTL 0xaa
#define UP_IOCTL 0xab
#define DOWN_IOCTL 0xac

enum servo_id {
    magnet = 0,
    piezo = 1
};

struct servo_settings {
    bool set;
    enum servo_id id;
    u32 active_duty_cycle;
    u32 inactive_duty_cycle;
};

#define IOWR_SETTINGS _IOWR(MAGIA_MAGIC, SETTINGS_IOCTL, struct servo_settings)
#define IOW_UP _IOW(MAGIA_MAGIC, UP_IOCTL, enum servo_id)
#define IOW_DOWN _IOW(MAGIA_MAGIC, DOWN_IOCTL, enum servo_id)

struct servo {
    struct servo_settings settings;
    struct pwm_device pwm;
};

struct servoctl {
    struct device *dev;
    struct gpio_desc *gpio_power;
    struct miscdevice misc_device;
    struct servo magnet_pwm_device;
    struct servo piezo_pwm_device;
    bool powered;
};

#define to_servoctl(p, m) container_of(p, struct servoctl, m);

static int servoctl_fopen(struct inode* node, struct file* file) {
    printk(KERN_INFO "Device file opened\n");
    return 0;
}

static int servoctl_frelease(struct inode* node, struct file* file) {
    printk(KERN_INFO "Device file closed \n");
    return 0;
}

static ssize_t servoctl_fwrite(struct file* file, const char *buf, size_t size, loff_t* off) {
    struct servoctl *chip = to_servoctl(file->private_data, misc_device);
    dev_info(chip->dev, "Write called\n");
    return 0;

}

static int servoctl_fread(struct file* file, char __user *buf, size_t size, loff_t* off) {
    struct servoctl *chip = to_servoctl(file->private_data, misc_device);
    dev_info(chip->dev, "Read called\n");
    return 0;
}

static int servoctl_move(struct servoctl* chip, enum servo_id id, bool up) {
    struct servo* motor;
    struct pwm_state* state;
    int ret;

    switch (id) {
    case magnet:
        motor = &chip->magnet_pwm_device;
        break;
    case piezo:
        motor = &chip->piezo_pwm_device;
    }

    pwm_get_state(&motor->pwm, state);

    dev_info(chip->dev, "Servo settings : %ui period, %ui duty cycle, %s\n",
             state->period, state->duty_cycle,
             state->enabled ? "enabled" : "disabled");


    state->duty_cycle = up ? motor->settings.active_duty_cycle : motor->settings.inactive_duty_cycle;
    state->enabled = true;

    ret = pwm_apply_state(&motor->pwm, state);
    if (ret < 0) {
        dev_err(chip->dev, "could not apply pwm state\n");
        return -EFAULT;
    }

    dev_info(chip->dev, "Servo settings applied\n");
}

static long servoctl_fioctl(struct file* file, unsigned int cmd, unsigned long arg) {
    int ret;
    struct servoctl *chip = to_servoctl(file->private_data, misc_device);

    switch (cmd) {
    case IOWR_SETTINGS:
    {
        struct servo_settings __user *uptr;
        struct servo_settings settings;

        uptr = (struct servo_settings __user *) compat_ptr(arg);

        if (copy_from_user(&settings, uptr, sizeof (settings)))
            return -EFAULT;

        if (settings.set) {
            // SET the corresponding servo settings to this structure...
            settings.set = false;
            switch (settings.id) {
            case magnet:
                chip->magnet_pwm_device.settings = settings;
                break;
            case piezo:
                chip->piezo_pwm_device.settings = settings;
                break;
            }
        }

        // GET the settings for wanted servo and set them to the user structure
        switch (settings.id) {
        case magnet:
            settings = chip->magnet_pwm_device.settings;
            break;
        case piezo:
            settings = chip->piezo_pwm_device.settings;
            break;
        }

        if (put_user(settings.active_duty_cycle, &uptr->active_duty_cycle))
            return -EFAULT;
        if (put_user(settings.inactive_duty_cycle, &uptr->inactive_duty_cycle))
            return -EFAULT;
        return 0;
    }
    case IOW_UP:
    case IOW_DOWN:
    {
        enum servo_id id;
        if (copy_from_user(&id, (void __user*) arg, sizeof (enum servo_id))) {
            dev_err(chip->dev, "Error getting servo ID for IOW_UP\n");
            return -EFAULT;
        }

        if (IOW_UP == cmd)
            servoctl_move(chip, id, true);
        else
            servoctl_move(chip, id, false);

        return 0;
    }
    default:
        break;
    }
    return -ENOTTY;
}

static struct file_operations servoctl_fops = {
    .owner = THIS_MODULE,
    .read = servoctl_fread,
    .write = servoctl_fwrite,
    .open = servoctl_fopen,
    .release = servoctl_frelease,
    .unlocked_ioctl = servoctl_fioctl,
};

static int servoctl_power(struct servoctl *chip, bool enable) {
    chip->powered = enable;
    return gpiod_set_value(chip->gpio_power, enable);
}

static int servo_subdevice_init(struct platform_device *client, struct servo* data, struct device_node* node) {
    int ret;

    /* Returns the PWM device parsed from the phandle and index specified in the “pwms”
     * property of a device tree node or a negative error-code on failure. Values parsed
     * from the device tree are stored in the returned PWM device object.
     * If con_id is NULL, the first PWM device listed in the “pwms” property will be
     * requested. Otherwise the “pwm-names” property is used to do a reverse lookup of
     * the PWM index. This also means that the “pwm-names” property becomes mandatory
     * for devices that look up the PWM device via the con_id parameter. */

    data->pwm = devm_of_pwm_get(&client->dev, node, NULL);
    if (IS_ERR(data->pwm)) {
        ret = PTR_ERR(data->pwm);
        if (-EPROBE_DEFER == ret) {
            dev_info(&client->dev, "probe defer on pwm\n");
            return -EPROBE_DEFER;
        }
        dev_err(&client->dev, "Unable to claim pwm\n");
        return ret;
    }

    ret = of_property_read_u32(node, "pwm-up", &data->settings.active_duty_cycle);
    if (ret < 0) {
        dev_alert(&client->dev, "Couldn't read pwm-up property. Set it through ioctl instead\n");
        data->settings.active_duty_cycle = 0;
    }

    ret = of_property_read_u32(node, "pwm-down", &data->settings.inactive_duty_cycle);
    if (ret < 0) {
        dev_alert(&client->dev, "Couldn't read pwm-down property. Set it through ioctl instead\n");
        data->settings.inactive_duty_cycle = 0;
    }

    return 0;
}

/**
 * @brief servoctl_init
 * @param client
 * @return
 */
static int servoctl_init(struct platform_device *client) {
    int ret;
    struct servoctl *data = platform_get_drvdata(client);

    data->gpio_power = devm_gpiod_get(&client->dev, "power", GPIOD_IN);
    if (IS_ERR(data->gpio_power)) {
        ret = PTR_ERR(data->gpio_power);
        if (-EPROBE_DEFER == ret) {
            dev_info(&client->dev, "probe defer on gpio_power\n");
            return -EPROBE_DEFER;
        }
        dev_err(&client->dev, "Unable to claim gpio \"power\"\n");
        return ret;
    }

    struct device_node *servo_node;
    servo_node = of_get_child_by_name(client->dev.of_node, "magnet");
    if (!servo_node) {
        dev_err(&client->dev, "Unable to read magnet node from DT\n");
        return -EINVAL;
    }

    ret = servo_subdevice_init(client, &data->magnet_pwm_device, servo_node);
    if (ret < 0) {
        dev_err(&client->dev, "unable to claim pwm for magnet node\n");
        return ret;
    }

    servo_node = NULL;
    servo_node = of_get_child_by_name(client->dev.of_node, "piezo");
    if (!servo_node) {
        dev_err(&client->dev, "Unable to read piezo node from DT\n");
        return -EINVAL;
    }

    ret = servo_subdevice_init(client, &data->piezo_pwm_device, servo_node);
    if (ret < 0) {
        dev_err(&client->dev, "Unable to claim pwm for piezo node\n");
        return ret;
    }

    servo_node = NULL;
    return 0;
}

static int servoctl_probe(struct platform_device *client) {
    int err;
    struct servoctl *chip;

    dev_info(&client->dev, "Probing servoctl driver\n");
    chip = devm_kzalloc(&client->dev, sizeof(*chip), GFP_KERNEL);
    if (!chip) {
        dev_err(&client->dev, "%s: Could not allocate memory\n", __func__);
        return -ENOMEM;
    }

    /* Setting up the link between the platform device and the servoctl device */
    chip->dev = &client->dev;
    platform_set_drvdata(client, chip);

    /* Setting up the miscellaneous character device. */
    chip->misc_device.minor = MISC_DYNAMIC_MINOR;
    chip->misc_device.name = MISC_NAME;
    chip->misc_device.fops = &servoctl_fops;

    err = misc_register(&chip->misc_device);
    if (err < 0) {
        dev_err(&client->dev, "%s: Could not register to misc framework\n", __func__);
        return err;
    }

    dev_info(&client->dev, "Servoctl set up, minor number %i\n", chip->misc_device.minor);

    /* Reading the device tree to get and initialize the gpios. */
    err = servoctl_init(client);
    if (err < 0) {
        dev_err(&client->dev, "%s: COuld not initialize servoctl\n", __func__);
        goto out_misc_dereg;
    }

    err = servoctl_power(chip, true);
    if (err < 0) {
        dev_alert(&client->dev, "Could not power up the servomotors\n");
    }

    return 0;

out_misc_dereg:
    misc_deregister(&chip->misc_device);
    dev_info(&client->dev, "Deregistered misc device\n");
    return err;
}

static int servoctl_remove(struct platform_device *client) {
    struct servoctl *chip = platform_get_drvdata(client);
    misc_deregister(&chip->misc_device);
    return 0;
}

static const struct of_device_id servoctl_dt_ids[] = {
    { .compatible = "magia,servoctl" },
    { /* sentinel */ },
};
MODULE_DEVICE_TABLE(of, servoctl_dt_ids);

static struct platform_driver servoctl_driver = {
    .probe = servoctl_probe,
    .remove = servoctl_remove,
    .driver = {
        .name = "servoctl",
        .of_match_table = servoctl_dt_ids,
    },
};

static int __init servoctl_driver_init(void) {
    return platform_driver_register(&servoctl_driver);
}
late_initcall(servoctl_driver_init);

static void __exit servoctl_driver_exit(void) {
    platform_driver_unregister(&servoctl_driver);
}
module_exit(servoctl_driver_exit);

MODULE_AUTHOR("Damien KIRK <damien.kirk@magia-diagnostics.com>");
MODULE_DESCRIPTION("Servomotor control driver (magnet + piezo)");
MODULE_LICENSE("GPL v2");
